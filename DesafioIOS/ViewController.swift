//
//  ViewController.swift
//  DesafioIOS
//
//  Created by Vinnycius Amancio on 30/11/16.
//  Copyright © 2016 com.ras. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class ViewController: UITableViewController {

    @IBOutlet var table: UITableView!
    
    var dataArray = [[String:AnyObject]]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        Alamofire.request("https://api.github.com/search/repositories?q=language:Java&sort=stars&page=1").responseJSON { (responseData) -> Void in
            if((responseData.result.value) != nil) {
                let swiftyJsonVar = JSON(responseData.result.value!)
                
                if let resData = swiftyJsonVar["items"].arrayObject {
                    self.dataArray = resData as! [[String:AnyObject]]
                }
                if self.dataArray.count > 0 {
                    self.table.reloadData()
                }
            }
        }
        
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataArray.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = table.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! RepositoryTableViewCell
        
        var dict = dataArray[indexPath.row]
        
        cell.repoName.text   = dict["name"            ] as? String ?? ""
        cell.repoDesc.text   = dict["description"     ] as? String ?? ""
        cell.lbFullName.text = dict["full_name"       ] as? String ?? ""
        
        cell.lbUsrName.text  = dict["owner"]?.value(forKey: "login") as? String ?? ""
        
        
        cell.lbFork.text!    = String(describing: dict["forks"           ] as! NSNumber) 
        cell.lbStarred.text! = String(describing: dict["stargazers_count"] as! NSNumber)
        
        if let strImage = dict["owner"]?.value(forKey: "avatar_url") as? String{
        
            do{
                
               var data = Data()
                
                data = try Data(contentsOf: URL(string: strImage)!)
                    
                cell.imgAvatar.image = UIImage(data: data)
               
                cell.imgAvatar.layer.cornerRadius = 35
                cell.imgAvatar.clipsToBounds = true
            }
            catch{
            
                print("erro ao baixar a imagem do avatar!")
            }
            
        }
        
        
        
        return cell
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

