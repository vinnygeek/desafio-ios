//
//  RepositoryTableViewCell.swift
//  DesafioIOS
//
//  Created by Vinnycius Amancio on 30/11/16.
//  Copyright © 2016 com.ras. All rights reserved.
//

import UIKit

class RepositoryTableViewCell: UITableViewCell {

    
    @IBOutlet weak var repoName: UILabel!
    
    @IBOutlet weak var repoDesc: UITextView!
    
    @IBOutlet weak var lbFork: UILabel!
    
    @IBOutlet weak var lbStarred: UILabel!
    
    @IBOutlet weak var imgAvatar: UIImageView!
    
    @IBOutlet weak var lbUsrName: UILabel!
    
    @IBOutlet weak var lbFullName: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
